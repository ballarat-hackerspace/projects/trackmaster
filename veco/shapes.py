from dataclasses import dataclass
from math import cos, sin, pi
from typing import List


@dataclass
class Geometry:
    """Base class for all geometries"""

    def translate(self, dx: float, dy: float) -> "Point":
        raise NotImplementedError("translate() not implemented for generic Geometry")

    def rotate(self, angle_radians: float) -> "Point":
        raise NotImplementedError("rotate() not implemented for generic Geometry")


@dataclass
class Point(Geometry):
    """A point in 2D space"""
    x: float = 0
    y: float = 0

    @staticmethod
    def origin():
        return Point(0, 0)

    def translate(self, dx: float, dy: float) -> "Point":
        """Move the point by a given amount"""
        self.x += dx
        self.y += dy
        return self

    def rotate(self, angle_radians: float) -> "Point":
        """Rotate the point by a given angle"""
        self.x = self.x * cos(angle_radians) - self.y * sin(angle_radians)
        self.y = self.x * sin(angle_radians) + self.y * cos(angle_radians)
        return self


@dataclass
class Line:
    """A line between two points"""
    start: Point
    end: Point

    def length(self):
        """Calculate the length of the line (Euclidean distance)"""
        return (
                (self.start.x - self.end.x) ** 2
                + (self.start.y - self.end.y) ** 2) ** 0.5

    def translate(self, dx: float, dy: float) -> "Line":
        """Move the line by a given amount"""
        self.start.translate(dx, dy)
        self.end.translate(dx, dy)
        return self

    def rotate(self, angle_radians: float) -> "Line":
        """Rotate the line by a given angle"""
        self.start.rotate(angle_radians)
        self.end.rotate(angle_radians)
        return self


@dataclass
class Polygon(Geometry):
    """A polygon defined by a list of points"""
    points: List[Point]

    def translate(self, dx: float, dy: float) -> "Polygon":
        """Move the polygon by a given amount"""
        for point in self.points:
            point.translate(dx, dy)
        return self

    def rotate(self, angle_radians: float) -> "Polygon":
        """Rotate the polygon by a given angle"""
        for point in self.points:
            point.rotate(angle_radians)
        return self


@dataclass
class Circle(Geometry):
    """A circle defined by a centre point and a radius"""
    centre: Point
    radius: float

    def translate(self, dx: float, dy: float) -> "Circle":
        """Move the circle by a given amount"""
        self.centre.translate(dx, dy)
        return self

    def rotate(self, angle_radians: float) -> "Circle":
        return self

    def convert_to_polygon(self, n_sides: int = 12) -> Polygon:
        """Convert a circle to a polygon with a given number of sides"""
        points = []
        for i in range(n_sides):
            angle = i * 2 * pi / n_sides
            points.append(Point(self.centre.x + self.radius * cos(angle),
                                self.centre.y + self.radius * sin(angle)))
        return Polygon(points=points)


def create_rectangle(width: float, height: float) -> Polygon:
    """Create a rectangle with a given width and height"""
    return Polygon(points=[
        Point(0, 0),
        Point(width, 0),
        Point(width, height),
        Point(0, height)
    ])


def create_square(side: float) -> Polygon:
    """Create a square with a given side length"""
    return create_rectangle(side, side)

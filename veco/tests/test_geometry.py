import pytest

from veco.shapes import create_rectangle

rectangle_area = [
    (0, 0, 1, 1, 1),

]


@pytest.mark.parametrize("x1, y1, x2, y2, area", rectangle_area)
def test_rectangle(x1, y1, x2, y2, area):
    assert create_rectangle(x2 - x1, y2 - y1).area() == area

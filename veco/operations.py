from typing import List

from veco.shapes import Polygon


def boundary_union(shapes: List[Polygon]) -> Polygon:
    """Calculate the union of the boundaries of the shapes"""
    boundaries = [shape.boundary for shape in shapes]
    return cascaded_union(boundaries)

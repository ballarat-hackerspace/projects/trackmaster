import textwrap
from pprint import pprint

import numpy as np
from shapely import unary_union

from tracklib import drawing
from tracklib.plotter import plot_shapes
from tracklib.tracks import TrackMaker
from tracklib.drawing import save_drawing


def main():
    maker = TrackMaker()

    all_shapes = []

    all_shapes.extend(maker.straight_track(140))

    for divider in [6, 4, 2]:
        all_shapes.extend(maker.curved_track(140, np.pi / divider))

    geometry = drawing.arrange(all_shapes)
    # geometry = unary_union(all_shapes)

    save_drawing('output/multi.svg', geometry, check_file_exists=False)

    fig = plot_shapes([geometry.simplify(0.1)], title='Straight Track')
    # fig.savefig('output/multi.svg')
    # fig.savefig("multi_polygon.svg", format="svg", transparent=True)

    with open('test.svg', 'w') as f:
        # specify margin in coordinate units
        margin = 5

        bbox = list(geometry.bounds)
        bbox[0] -= margin
        bbox[1] -= margin
        bbox[2] += margin
        bbox[3] += margin

        width = bbox[2] - bbox[0]
        height = bbox[3] - bbox[1]

        # transform each coordinate unit into "scale" pixels
        scale = 1

        props = {
            'version': '1.1',
            'baseProfile': 'full',
            'width': '{width:.0f}mm'.format(width=width * scale),
            'height': '{height:.0f}mm'.format(height=height * scale),
            'viewBox': '%.1f,%.1f,%.1f,%.1f' % (bbox[0], bbox[1], width, height),
            'xmlns': 'http://www.w3.org/2000/svg',
            'xmlns:ev': 'http://www.w3.org/2001/xml-events',
            'xmlns:xlink': 'http://www.w3.org/1999/xlink'
        }

        pprint(props)

        f.write(textwrap.dedent(r'''
            <?xml version="1.0" encoding="utf-8" ?>
            <svg {attrs:s}>
            {data:s}
            </svg>
        ''').format(
            attrs=' '.join(['{key:s}="{val:s}"'.format(key=key, val=props[key]) for key in props]),
            data=geometry.svg()
        ).strip())

if __name__ == '__main__':
    main()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/

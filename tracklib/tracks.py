from dataclasses import dataclass
from typing import List

import geopandas as gpd
import numpy as np
import shapely.geometry as geom
from shapely import Polygon, Point, unary_union
from shapely.affinity import translate, rotate


@dataclass
class TrackMaker:
    """Environment for generating tracks with a given set of parameters

    All measurements in mm, always and forever.
    """

    wood_thickness: float = 3.0
    track_width: float = 3.0

    rail_width: float = 1.9
    rail_gap: float = 16.7
    rail_length_before_sleeper: float = 5

    sleeper_width: float = 3.8
    sleeper_overhang_single_side: float = 3.15
    sleeper_gap: float = 10.2

    notch_radius: float = 3
    notch_offset_from_rail: float = 5.1
    notch_siding: float = 2.0
    notch_female_width: float = 7.0
    notch_female_height: float = 8.7
    notch_female_inner: float = 5.1

    curve_sides: int = 120

    def curved_track(self, inner_radius: float = 100, curve_radians: float = np.pi / 2) -> List[Polygon]:
        shapes = []
        bottom_layer_shapes = []


        inner_sleeper_radius = inner_radius
        # Compute the arc length to the start of the first sleeper based on self.rail_length_before_sleeper

        arc_length_to_sleeper = self.rail_length_before_sleeper / inner_sleeper_radius
        print(f"Arc length to sleeper: {arc_length_to_sleeper}")
        print(f"Curve radians: {curve_radians}")

        arc_gap_between_sleepers = self.sleeper_gap / inner_sleeper_radius
        print(f"Angle gap between sleepers: {arc_gap_between_sleepers}")

        # TODO: Need to adjust the sleeper width slightly to get a consistent gap between sleepers

        inner_sleeper_radius = inner_radius - self.sleeper_overhang_single_side * 2

        # Place sleepers
        angle_radians = 0
        sleeper_angle_positions = list(np.arange(arc_length_to_sleeper,
                                                    curve_radians - arc_length_to_sleeper,
                                                    arc_gap_between_sleepers))
        sleeper_angle_positions.append(curve_radians - arc_length_to_sleeper)
        for i, angle_radians in enumerate(sleeper_angle_positions):
            print(f"Angle: {angle_radians}")
            if i == 0:
                # Starting notch
                male_notch = self.male_notch()
                male_notch = translate(male_notch,
                                       self.notch_offset_from_rail / 4,  # TODO: what should this actually be?
                                       inner_radius - self.sleeper_overhang_single_side
                                       - self.rail_width
                                       + self.sleeper_length / 2
                                       + self.notch_siding / 2
                                       )
                bottom_layer_shapes.append(male_notch)

            # Create the sleeper
            sleeper = self.sleeper()
            # Move the sleeper to the correct position
            sleeper = translate(sleeper, 0, inner_radius - self.sleeper_overhang_single_side)
            # Rotate the sleeper to the correct angle
            sleeper = rotate(sleeper, -angle_radians, origin=(0, 0), use_radians=True)

            # Append a copy of sleeper
            bottom_layer_shapes.append(sleeper)

        # Add female notch tangent to the end of the track
        female_notch = self.female_notch()
        female_notch = translate(
            female_notch,
            self.notch_offset_from_rail / 2,  # TODO: what should this actually be?
            inner_radius - self.sleeper_overhang_single_side
            - self.rail_width
            + self.sleeper_length / 2
            - self.notch_female_width / 2
        )
        female_notch = rotate(female_notch, -angle_radians, origin=(0, 0), use_radians=True)
        bottom_layer_shapes.append(female_notch)


        # Create the inner side of the inner rail
        print(f"Final angle: {angle_radians} / {curve_radians}")
        inner_rail = self.curved_rail(inner_radius, curve_radians)
        # Create the outer side of the inner rail
        outer_radius = inner_radius + self.rail_gap
        outer_rail = self.curved_rail(outer_radius, curve_radians)

        # Append a copy of inner_rail
        bottom_layer_shapes.append(inner_rail)
        bottom_layer_shapes.append(outer_rail)

        bottom_layer = union(gpd.GeoSeries(bottom_layer_shapes))
        bottom_layer = unary_union(bottom_layer_shapes)

        # Move outer rail to the right by the track width
        outer_rail = translate(outer_rail, self.rail_gap, 0)


        shapes.append(bottom_layer)
        return shapes

    def straight_track(self, track_length: float = 140) -> List[Polygon]:
        """Generate a straight track of a given length"""
        shapes = []
        # There are two layers to the track. The top layer which is just rails,
        # and the bottom layer with also includes sleepers.

        # Add two rails for the top layer
        shapes.append(self.straight_rail(track_length))
        # Add a copy of rail
        shapes.append(self.straight_rail(track_length))

        # Build the bottom layer
        bottom_layer_shapes = []
        top_rail = self.straight_rail(track_length)
        # Drop top rail by the overhang
        top_rail = translate(top_rail, 0, self.sleeper_overhang_single_side)

        # Bottom rail
        bottom_rail = self.straight_rail(track_length)
        # Move the bottom rail down by the track width
        bottom_rail = translate(bottom_rail, 0, self.sleeper_overhang_single_side + self.rail_width + self.rail_gap)
        bottom_layer_shapes.append(top_rail)
        bottom_layer_shapes.append(bottom_rail)

        # Add sleepers
        for i, startx in enumerate(np.arange(self.rail_length_before_sleeper,
                                             track_length - self.rail_length_before_sleeper,
                                             self.sleeper_gap + self.sleeper_width)):
            if i == 0:
                # Starting notch
                male_notch = self.male_notch()
                male_notch = translate(male_notch, startx - self.notch_offset_from_rail,
                                       self.sleeper_overhang_single_side + self.rail_width + self.rail_gap / 2)
                bottom_layer_shapes.append(male_notch)

            sleeper = self.sleeper()
            sleeper = translate(sleeper, startx, 0)
            bottom_layer_shapes.append(sleeper)

        print(f"Extra after track is {track_length - startx - self.sleeper_width}")

        # Add female notch at the end
        female_notch = self.female_notch()
        dx = startx + self.sleeper_width - 0.1
        dy = self.sleeper_overhang_single_side + self.rail_width + self.rail_gap / 2.0 - self.notch_female_height / 2
        female_notch = translate(female_notch, dx, dy)
        bottom_layer_shapes.append(female_notch)

        #  Calculate the union of the boundaries of the bottom layer
        bottom_layer = union(gpd.GeoSeries(bottom_layer_shapes))
        shapes.extend(bottom_layer)

        return shapes

    def straight_rail(self, track_length: float) -> Polygon:
        """Generate a rail of a given length"""
        return create_rectangle(track_length, self.rail_width)

    @property
    def sleeper_length(self):
        """Length of a sleeper"""
        return (
                self.sleeper_overhang_single_side +
                self.rail_width +
                self.rail_gap +
                self.rail_width +
                self.sleeper_overhang_single_side
        )

    def sleeper(self):
        """Generate a sleeper"""
        return create_rectangle(self.sleeper_width, self.sleeper_length)

    def male_notch(self):
        circle = Point(0, 0).buffer(self.notch_radius)
        # Add a small offset box
        box = create_rectangle(self.notch_offset_from_rail, self.notch_siding * 2)
        box = translate(box, 0, -self.notch_siding)
        # Combine the two
        shapes = union(gpd.GeoSeries([circle, box]))
        if len(shapes) > 1:
            raise ValueError("Notch is not a single shape")
        return shapes[0]

    def female_notch(self):
        notch = create_rectangle(self.notch_female_width, self.notch_female_height)
        # Add a small offset box
        small_box = create_rectangle(self.notch_female_width, self.notch_female_inner)
        centre_dy = (self.notch_female_height - self.notch_female_inner) / 2
        small_box = translate(small_box, 0, centre_dy)
        notch = difference(notch, small_box)

        circle_hole = Point(self.notch_female_inner,
                            self.notch_female_height / 2
                            ).buffer(self.notch_radius)
        notch = difference(notch, circle_hole)

        return notch

    def curve(self, radius, curve_radians):
        # The coordinates of the arc
        theta = np.linspace(0, curve_radians, self.curve_sides)
        x = radius * np.sin(theta)
        y = radius * np.cos(theta)
        print(x[:5], y[:5])
        return np.column_stack([x, y])

    def curved_rail(self, inner_radius, curve_radians):

        # Create the arc for the inner side
        inner_arc = self.curve(inner_radius, curve_radians)
        # Create the arc for the outer side
        outer_arc = self.curve(inner_radius + self.rail_width, curve_radians)
        # Reverse the outer arc
        outer_arc = np.flip(outer_arc, axis=0)
        # concat inner and outer
        arc_points = np.concatenate([inner_arc, outer_arc])

        arc = geom.LineString(arc_points)
        return Polygon(arc)


def create_rectangle(width, height) -> Polygon:
    """Create a rectangle with a given width and height"""
    return Polygon([
        (0, 0),
        (width, 0),
        (width, height),
        (0, height)
    ])


def difference(main, cutout):
    result_geom = main.difference(cutout)
    return result_geom


def union(gdf) -> List[Polygon]:
    """Calculate the union of the boundaries of the shapes"""
    if isinstance(gdf, gpd.GeoSeries):
        gdf = gpd.GeoDataFrame(geometry=gdf).reset_index()
    gdf_merge = gdf.dissolve().explode(index_parts=True)
    gdf_clean = gpd.sjoin(gdf_merge, gdf, how='inner', predicate='intersects')
    gdf_clean['AREA'] = gdf_clean['geometry'].apply(lambda x: x.area)
    gdf_clean = gdf_clean.groupby('AREA')
    gdf_clean = gdf_clean.agg({'geometry': 'first'})
    gdf_clean = gdf_clean.reset_index(level='AREA').drop(columns=['AREA'])

    return list(gdf_clean['geometry'])

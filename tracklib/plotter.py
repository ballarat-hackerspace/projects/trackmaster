import matplotlib.pyplot as plt
from shapely.plotting import plot_polygon


def plot_shapes(shapes, title='', figsize=(10, 10), dpi=90):
    """Plot a figure with a given title and size"""
    fig = plt.figure(1, figsize=figsize, dpi=dpi)

    # 1: simple line
    ax = fig.add_subplot(111)

    for shape in shapes:
        plot_polygon(shape, ax=ax, add_points=False, color='black')

    # ax.set_title(title)
    ax.axis('equal')

    # Turn off axes, labels, and ticks
    ax.axis('off')
    ax.set_xticks([])
    ax.set_yticks([])
    # Make background transparent
    fig.patch.set_visible(False)
    ax.axis('off')

    plt.show()
    return fig

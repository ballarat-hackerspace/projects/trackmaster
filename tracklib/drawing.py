import os
from typing import List

import svgwrite
from shapely.affinity import translate
from shapely.geometry import MultiPolygon, LineString
from shapely.geometry.base import BaseGeometry
from shapely.ops import unary_union


def arrange(shapes: List[BaseGeometry]) -> MultiPolygon:
    """Arrange a list of shapes into a single geometry"""
    # TODO: Minimise the wastage
    # Initialize y_offset to 0.
    y_offset = 0  # TODO: Reset to 0

    # Create an empty list to store the new geometry objects.
    new_geoms = []

    # Loop over the geometry objects.
    for geom in shapes:
        # Get the height of the bounding box of the geometry.
        height = geom.convex_hull.bounds[3] - geom.convex_hull.bounds[1]
        # Translate the geometry such that its lower edge is at y_offset.
        new_geom = translate(geom, yoff=y_offset - geom.bounds[1])
        # Add the translated geometry to the list.
        new_geoms.append(new_geom)

        # Increase the y_offset by the height of the geometry, plus a little extra to ensure there's no overlap.
        y_offset += height + 1

    # Return the union of the translated geometries.
    return unary_union(new_geoms)


def save_drawing(filename: str, multipolygon: BaseGeometry,
                 check_file_exists: bool = True,
                 fill_color: str = 'black'):
    if check_file_exists:
        if os.path.exists(filename):
            raise FileExistsError(f'File {filename} already exists')

    svg = multipolygon._repr_svg_()
    with open(filename, 'w') as f:
        f.write(svg)


def save_drawing_old(filename: str, multipolygon: BaseGeometry, check_file_exists: bool = True, fill_color: str = 'black'):
    """Save a drawing to a file"""
    if check_file_exists:
        if os.path.exists(filename):
            raise FileExistsError(f'File {filename} already exists')

    # Set drawing size to polygon size, in mm
    width, height = multipolygon.convex_hull.bounds[2:]
    dwg = svgwrite.Drawing(filename, profile='tiny',
                           size=(width * svgwrite.mm, height * svgwrite.mm),
                           viewBox=f'0 0 {width} {height}')

    for polygon in multipolygon:
        if isinstance(polygon, LineString):
            points = list(polygon.coords)
            dwg.add(dwg.polyline(points=points, stroke=fill_color, fill='none'))
        else:
            points = list(polygon.exterior.coords)
            dwg.add(dwg.polygon(points=points, fill=fill_color))

    dwg.save()
